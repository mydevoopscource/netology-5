from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify

app = Flask(__name__)
api = Api(app)

class Info(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'My new Flask API'}
class Fail(Resource):
    def get(self):
        return {'version':3 'fail': True}

api.add_resource(Info, '/get_info') # Route_1
api.add_resource(Fail, '/failure')

if __name__ == '__main__':
     app.run(host='0.0.0.0', port='5290')
